# Long Term Storage Project
# Getting started 

## Publickeys
Publickeys added to this folder ```shared\authorized_keys.d``` will be added for authorization at the hypervisor and vm.

## Creating an diskimage
### Packer
1.Create a ed25519 host key with follwing name "ssh_host_ed25519_key" accordingly "ssh_host_ed25519_key.pub" and move it into prvisioning/secrets.

```shell
cd alxmonitoring/offline/packer/provisioning
mkdir secrets
cd secrets
ssh-keygen -t ed25519
```

2.Now you are ready to build the diskimage. Change into ```alxmonitoring/offline/packer``` folder and run the following command.

```shell
./build.sh archlinux-monitoring.json
```

### Ansible

4.Create the necessary TLS certificates with following script.

```shell
alxmonitoring/playbooks/files/openssl/script/generate.sh
```

5.Start the newly created diskimage with your local hypervisor and obtain the assigned ip address. e.g.:

```shell
arp -i %virtual_network_name% -n
```

6.Change into ```alxmonitoring``` folder and change in ```inventory.yml```the variable 'ansible_host' to your vm's ip address.

7.Before Ansible can configure and provision the vm, we need to set an sudo password for user anible. e.g:

```shell
ssh root@%ip% -i %your_key%
passwd ansible
```

8.To configure and provision the newly created vm, run follwing ansible-playbook command.

```shell
ansible-playbook alxmonitoring.yml --extra-vars "ansible_sudo_pass=%sudopassword%"
```

## Uploading diskimage to hypervisor
To upload your created diskimage to hypervisor into following folder. ```/var/lib/ltsvms/<dev|op>```

```shell
scp -i ~/.ssh/<key> diskimage/archlinux-monitoring root@mon-storage-test:/var/lib/ltsvms/dev
```

## Managing vm with virsh
### Connect virsh
```
virsh connect qemu:///system
```
Connects locally as the root user to the daemon supervising guest virtual machines on the KVM hypervisor.

### List all KVM guests (vms)
```
virsh list --all
```

### Install new VM
```
virt-install --help
```

### Show/Edit KVM vm configuration (libvirt-XML file)
```
virsh <dumxml|editxml> VM_NAME
```
### Start/Stop/Reboot
```
virsh <start|reboot|shutdown> VM_NAME
```
### Delete VM
```
virsh destroy VM_NAME && virsh undefine VM_NAME
```