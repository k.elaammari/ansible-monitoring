# notes

## iptables

iptables -t nat -D PROROUTING %number

## Hypervisor

virsh net-list
virsh net-info %name%
virsh net-dumpxml %name%
virsh net-dhcp-leases %name%

virsh dumpxml %vmname%

## ssh

ssh-keygen -l -f %file%      "Prints info and hash"
