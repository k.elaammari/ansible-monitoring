#!/bin/bash
if [[ "$(dirname $BASH_SOURCE)" != "." ]];then
    exit
fi

# remove old packer output
rm -rf outputdir

# build new image
export PACKER_KEY_INTERVAL=10ms
packer build $@

# copy new image to seperate folder and create a backup file aleary exist
if [[ $? == 0 ]];then
    mkdir -p diskimage
    cp --backup=t outputdir/archlinux-monitoring diskimage/archlinux-monitoring
fi
echo "End: `date`"
